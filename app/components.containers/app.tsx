import React, {Component} from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';
import { addNavigationHelpers, NavigationActions } from 'react-navigation';
import getRootReducer from '../redux.reducers';
import {RootNavigator, rootNavigatorReducer} from '../router/MainNavigator';
import {mainTabNavigatorReducer} from '../router/MainTabs';

import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import promise from 'redux-promise';
import { composeWithDevTools } from 'remote-redux-devtools';

type AppWithNavigationProps = {
    tokenHasChanged?: (token: string) => {},
    initialScreenHasChanged?: (initialScreen: string) => {},
    initialRoute?: string,
    token?: string,
    dispatch?: Function,
    nav?: any
}

//create redux store
const logger = createLogger();
const middleware = composeWithDevTools(applyMiddleware(thunk, promise, logger));
const store = createStore(getRootReducer(rootNavigatorReducer, mainTabNavigatorReducer), middleware);

@connect( (state) => ({ nav: state.nav }) )
class AppWithNavigation extends React.Component<any, any> {
    props: AppWithNavigationProps;
    
    render() {
        return <RootNavigator navigation={addNavigationHelpers({
            dispatch: this.props.dispatch,
            state: this.props.nav
        })} />
    }
}

export default class Root extends Component<any, any> {
    render() {
        return (
            <Provider store={store}>
                <AppWithNavigation />
            </Provider>
        );
    }
}
