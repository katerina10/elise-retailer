import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    View
} from "react-native";
import Camera from "react-native-camera";
import * as routeNames from "../../router/routeNames";
import HttpRequestService from "../../services.common/HttpRequestService";
import StoresService from '../../services.common/StoresService';
import { SERVER_URL } from '../../config';

export default class CameraScanner extends Component<any, any> {
    lockScannerAfterSuccessfullScan() {
        let lockScan = false;
        return async (data, bounds) => {
            if (!lockScan) {
                console.log('read qr code: ', data);
                lockScan = !lockScan;
                //todo: remove this call once the Retailer app is ready
                const stores = await StoresService.listStores();
                HttpRequestService.post(`${SERVER_URL}/user/checkin`, {
                    userId: '1',
                    storeId: stores[0].id,
                    money: '500'
                });
                this.props.navigation.dispatch({type: routeNames.MAIN_TABS, reset: true});
            }
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Camera
                    ref={cam => (this.camera = cam)}
                    style={styles.preview}
                    aspect={Camera.constants.Aspect.fill}
                    keepAwake={true}
                    onBarCodeRead={this.lockScannerAfterSuccessfullScan()}
                >
                    <Text
                        style={styles.capture}
                        onPress={() =>
                            this.props.navigation.dispatch({
                                type: routeNames.MAIN_TABS
                            })}
                    >
                        {" "}[BACK]{" "}
                    </Text>
                </Camera>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row"
    },
    preview: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center"
    },
    capture: {
        flex: 0,
        backgroundColor: "#fff",
        borderRadius: 5,
        color: "#000",
        padding: 10,
        margin: 40
    }
});
