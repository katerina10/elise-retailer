import React, { Component } from 'react';
import {
    View,
    StyleSheet
} from 'react-native';
import Button from '../../components/Button';
import { CAMERA_SCAN } from '../../router/routeNames';
import { connect } from 'react-redux';

@connect(null, {})
export default class IntroScreen extends React.Component<any, any> {
    props: any;

    camera() {
        this.props.navigation.dispatch({type: CAMERA_SCAN, reset: true});
    }

    render() {
        return (
            <View style={styles.screen}>
                <Button
                    label              = "SCAN"
                    borderColor        = "#FCC051"
                    labelColor         = "#FFFFFF"
                    backgroundColor    = "#FCC051"
                    extraStyles        = {{width: 200, alignSelf: 'center'}}
                    applyShadow
                    onPress            = {() => {this.camera()}}>
                </Button>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: '#000000'
    }
});

