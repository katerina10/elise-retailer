import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
import { INTRO_SCREEN } from '../../router/routeNames';
import { connect } from 'react-redux';

@connect(null, {})
export default class LoadingScreen extends React.Component<any, any> {
    props: any;

    async componentWillMount() {
        this.props.navigation.dispatch({type: INTRO_SCREEN, reset: true});
    }

    render() {
        return (
            <View style={styles.screen}>
                <Text>Loading app...</Text>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
