'use strict';

import * as React from 'react';
import {
    Text,
    View,
    TouchableHighlight,
    StyleSheet
} from 'react-native';

/**
 * @type {Object}
 * @property {string} boderColor - set the border color
 * @property {string} backgroundColor - set the background color
 * @property {string} labelColor - the color of the text of the button
 * @property {string} label - the text of the button
 * @property {Function} onPress - a callback to execute when clicking the button
 */
type Props = {
    borderColor?: string,
    backgroundColor?: string,
    labelColor?: string,
    borderWidth?: number,
    borderRadius?: number,
    label: string,
    underlayColor? :string,
    applyShadow?: boolean,
    extraStyles?: React.FlexStyle,
    extraLabelStyles?: React.FlexStyle,
    onPress?: Function
}

export default class Button extends React.Component<any, any> {
    public props: Props;
    protected userStyle: any;

    /**
     * @name handleClick
     * @description
     * Calls the user callback when the button is clicked
     */
    handleClick() {
        if (this.props.onPress) {
            this.props.onPress();
        }
    }

    render() {
        //style to be merged with component's own internal styles
        this.userStyle = {
            borderWidth: this.props.borderWidth || 1,
            borderColor: this.props.borderColor,
            borderRadius: this.props.borderRadius,
            backgroundColor: this.props.backgroundColor
        }

        return (
            <TouchableHighlight onPress={() => this.handleClick()}>
                <View style={[styles.container, this.userStyle, this.props.extraStyles, this.props.applyShadow ? styles.buttonShadow : {}]}>
                    <Text style={[styles.label, {color: this.props.labelColor}, this.props.extraLabelStyles]}>{this.props.label}</Text>
                </View>
            </TouchableHighlight>
        );
    }
}

const styles = StyleSheet.create<any>({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderWidth: 1
    },
    buttonShadow: {
        shadowColor:'#4D6662',
        shadowOffset:{
            width: 3,
            height: 3,
        },
        shadowOpacity: 0.8,
        shadowRadius: 4,
    },
    label: {
        fontSize: 20,
        fontWeight: '900',
        textAlign: 'center'
    }
});
