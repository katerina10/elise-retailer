import {combineReducers, ReducersMapObject} from 'redux';
import TabIndexReducer from './TabIndexReducer';

/**
 * @description
 * a map of properties and functions under each property.
 * Each key corresponds to a property of our state and each function under each key,
 * is actually the reducer function which creates new state for this specific property.
 */
const reducersMap: ReducersMapObject = {
    tabIndex: TabIndexReducer,
};

/**
 * @description
 * Each time an action is dispatched, `combineReducers` will make sure
 * to call each reducer function for each key we have in the `reducersMap`.
 * Each reducer knows internally whether it should handle this action and
 * produce a new piece of state. Essentially, for each action we `.reduce()`
 * all individual reducers functions to a new piece of state. Hence the
 * `reducer` terminology.
 */
export default function getRootReducer(navigationReducer, mainTabReducer) {
    return combineReducers({...reducersMap, nav: navigationReducer, tabBarNav: mainTabReducer});
}
