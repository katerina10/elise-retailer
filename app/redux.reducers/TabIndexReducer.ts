export default (state = 0, action) => {
    if (action.type === 'set_tab_index') {
        return action.payload;
    }
    return state;
}