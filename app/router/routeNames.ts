export const LOADING_SCREEN = '@goToScreen/loading-screen';
export const INTRO_SCREEN = '@goToScreen/intro-screen';
export const CAMERA_SCAN = '@goToScreen/camera-scan';
export const MAIN_TABS = '@goToScreen/main-tabs';
