export const ROUTE_TYPES = {
    SCREEN: 'screen',
    MODAL: 'modal',
    CONTAINER: 'container'
};

/**
* @property {string} name The name of the route
* @property {JSXElement} Component The top level screen component
* @property {screen|modal} type Determines whether this screen component should be displayed as a modal
* @property {boolean} reset When true, it will completely reset navigation history
* @property {object} params Optional route parameters
*/
export interface IRouteStatic {
    name?: string,
    screen: React.ComponentClass<any>,
    type?: string,
    reset?: boolean
    params?: any
}

export type IRouteMapStatic = {
    [propName: string]: IRouteStatic;
}
