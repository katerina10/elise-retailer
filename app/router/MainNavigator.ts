import LoadingScreen from '../components.screens/loading-screen/LoadingScreen';
import IntroScreen from '../components.screens/intro-screen/IntroScreen';
import CameraScreen from '../components.screens/camera-screen/CameraScreen';
import * as routeNames from './routeNames';
import {
    StackNavigator,
    TabNavigator,
    NavigationActions,
    NavigationStackViewConfig
} from 'react-navigation';
import { ROUTE_TYPES, IRouteMapStatic } from './routes.dt';
import { MainTabNavigatorComponent } from './MainTabs';

const RootNavigatorScreenStack: IRouteMapStatic = {
    [routeNames.LOADING_SCREEN]: {
        name: routeNames.LOADING_SCREEN,
        screen: LoadingScreen,
        type: ROUTE_TYPES.SCREEN,
        params: {}
    },
    [routeNames.INTRO_SCREEN]: {
        name: routeNames.INTRO_SCREEN,
        screen: IntroScreen,
        type: ROUTE_TYPES.SCREEN,
        params: {}
    },
    [routeNames.CAMERA_SCAN]: {
        name: routeNames.CAMERA_SCAN,
        screen: CameraScreen,
        type: ROUTE_TYPES.SCREEN,
        params: {}
    },
    [routeNames.MAIN_TABS]: {
        screen: MainTabNavigatorComponent
    }
};

/**
 * prepare root navigator and reducer
 */

//RootNavigator. This is the topmost navigator of our app. Everything else is nested under it
const NavigatorConfig: NavigationStackViewConfig = {headerMode: 'none'};
export const RootNavigator = StackNavigator(
    RootNavigatorScreenStack,
    NavigatorConfig
);

//Root Navigator Reducer
const initialState = RootNavigator.router.getStateForAction(RootNavigator.router.getActionForPathAndParams(routeNames.LOADING_SCREEN));
export const rootNavigatorReducer = (state = initialState, action) => {
    //main forward route action
    const forwardAction = NavigationActions.navigate({routeName: action.type});

    //if an action is flagged as a reset action then
    //  a. first reset navigation stack
    //  b. then perform a normal forward action
    //otherwise just perform the normal forward action
    const routeAction = (action.reset) ?
        NavigationActions.reset({index: 0, actions: [forwardAction]}) :
        forwardAction;

    //pass the route action to the router and obtain a new router state
    const newState = RootNavigator.router.getStateForAction(
        routeAction,
        state
    );

    //return our new router state and let redux continue its cycle
    return newState || state;
};
