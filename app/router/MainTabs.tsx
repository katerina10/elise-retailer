import {
    TabNavigator,
    NavigationActions,
    addNavigationHelpers
} from 'react-navigation';

import * as React from 'react';
import {
    View,
    Text
} from 'react-native';
import { connect } from 'react-redux';

/**
 * todo: no longer needed when we have screens for all our tabs
 * @param  {[type]} tabTitle [description]
 * @param  {[type]} bgColor  [description]
 */
const demoTab = (tabTitle, bgColor) => (
    () => ({
        ...React.Component.prototype,
        render() {
            return (
                <View style={{flex:1, backgroundColor: bgColor, paddingTop: 65, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{color: '#ffffff', fontSize: 28}}>{tabTitle}</Text>
                </View>
            )
        }
    })
);

export const MainNavigationTabsConfig = {
    'events': {
        screen: demoTab('Events Tabs', 'red'),
        navigationOptions: {
            tabBarLabel: 'Events'
        }
    }
};

const MainTabNavigator = TabNavigator(
    MainNavigationTabsConfig,
    {
        tabBarOptions: {
            activeTintColor: '#56606c'
        }
    }
);

// Main Tabs Reducer
export const mainTabNavigatorReducer = (state, action) => {
    const newState = MainTabNavigator.router.getStateForAction(
        action,
        state
    );
    return newState || state;
};

@connect( 
    state => ({ tabBarNav: state.tabBarNav })
)
export class MainTabNavigatorComponent extends React.Component<any, any> {
    props: {
        dispatch: Function,
        tabBarNav: any,
        showVerifyPopup: boolean
    };

    render() {
        return (
            <MainTabNavigator navigation={addNavigationHelpers({
                dispatch: this.props.dispatch,
                state: this.props.tabBarNav
            })} />
        );
    }
}
