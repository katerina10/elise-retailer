/// <reference path="globals/react-native/index.d.ts" />
/// <reference path="globals/react/index.d.ts" />
/// <reference path="globals/whatwg-fetch/index.d.ts" />
/// <reference path="modules/lodash/index.d.ts" />

declare module 'react-native-vector-icons/FontAwesome' {
    interface IconStatic extends React.ComponentClass<any> {
        name: string;
        size: number;
    }
    let Icon: IconStatic;
    export default Icon;
}

declare module 'react-native-scrollable-tab-view' {
    let ScrollableTabView: any
    export default ScrollableTabView;
}

declare module 'react-native-md-textinput' {
    let textField: any;
    export default textField;
}

declare module 'react-native-app-intro' {
    let appIntro: any;
    export default appIntro;
}

declare var module;
