import {SERVER_URL} from '../config';
import HttpRequestService from './HttpRequestService';

class StoresService {
    async listStores() {
        const url = `${SERVER_URL}/elise-stops`;
        let json = await HttpRequestService.get(url);
        return json.eliseStops;
    }

    dispatch(type: string, payload: any) {
        return (dispatch) => {
            dispatch({
                type: type,
                payload: payload
            });
        };
    }
}

export default new StoresService();
