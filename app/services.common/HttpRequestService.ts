export default new class HttpRequestService {
    /**
     * @description
     * Posts data to server. Adds token to the headers if token exists.
     * @return {Promise<boolean>}
     */
    async post(url:string, data:any): Promise<any> {
        let headers = await this.getRequestHeaders();

        try {
            const response = await fetch(
                url,
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(data)
                }
            );

            return await response.json();
        } catch(error) {
            console.error(error);
            return null;
        }
    }

    /**
     * @description
     * Gets data from server. Adds token to the headers if token exists.
     * @return {Promise<boolean>}
     */
    async get(url:string): Promise<any> {
        let headers = await this.getRequestHeaders();

        try {
            const response = await fetch(
                url,
                {
                    method: 'GET',
                    headers: headers
                }
            );

            return await response.json();
        } catch(error) {
            console.error(error);
            return null;
        }
    }

    /**
     * Constructs and returns the request headers by adding user-token if found.
     * @returns {{Accept: string, Content-Type: string, AUTHORIZATION?: string}}
     */
    async getRequestHeaders(): Promise<any> {
        // const token = await AuthService.getToken();
        let headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        };
        // if (token) {
        //     headers['AUTHORIZATION'] = 'Bearer' + token;
        // }
        return headers;
    }
}
