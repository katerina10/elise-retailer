/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import {
    AppRegistry,
} from 'react-native';
import Root from './built/components.containers/app';

AppRegistry.registerComponent('eliseRetailer', () => Root);
