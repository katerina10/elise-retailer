'use strict';

const gulp = require('gulp');
const ts = require('gulp-typescript');
const exec = require('child_process').exec;
const del = require('del');
const rename = require("gulp-rename");
const template = require('gulp-template');
const argv = require('minimist')(process.argv.slice(2));

//get typescript project
const tsProject = ts.createProject('tsconfig.json');

//get template data
let SERVER_URL = argv.server;
if (!SERVER_URL) SERVER_URL = 'http://localhost:9999';

//helper methods
const copyStaticFiles = () => {
    gulp
        .src([
            'app/**/img/**/*',
            'app/fonts/**/*'
        ], {base: 'app'})
        .pipe(gulp.dest('built/'))
}

gulp.task('clean', function (cb) {
    return del([
        'built/',
        'app/config.ts'
    ]);
});

gulp.task('config', ['clean'], () => {
    gulp.src(['app/config.template.ts'])
        .pipe(template({baseUrl: SERVER_URL}))
        .pipe(rename('config.ts'))
        .pipe(gulp.dest('app'));
});

gulp.task('ts', ['config'], function (cb) {
    exec('tsc -p ./', function (err, stdout, stderr) {
        cb();
    });
});

/**
 * copy -> ts -> config -> clean
 */
gulp.task('move:assets', ['ts'], copyStaticFiles);

/**
 * copy task without any dependencies
 */
gulp.task('copy', copyStaticFiles);

/**
 * build -> move:assets -> ts -> config -> clean
 * todo: Consider introducing a ./tmp intermediate folder to copy assets to. That way we can parallelize the `ts` and `copy tasks`
 * ======== MAIN BUILD TASK =========
 * 1. Will delete all generated folders and files
 * 2. Will generate a new config.ts in the /app folder
 * 3. Will compile all app/ typescript files to a new target dir `built`
 * 4. Will move some static assets from `app/` to the newly created `built` folder
 */
gulp.task('build', ['move:assets']);

gulp.task('watch', ['build'], function() {
    gulp.watch('app/**/*.ts', ['build']);
    gulp.watch('app/**/*.tsx', ['build']);
});

gulp.task('default', ['build']);
