#!/usr/bin/env node
const fs = require('fs');
const rootPath = require('process').cwd();
const placeholders = require('placeholders')({});
const argv = require('minimist')(process.argv.slice(2));

//get command level arguments
const SERVER_URL = (argv.server) ? argv.server : 'localhost:9999';
const PROTOCOL = argv.protocol || 'http';
const SERVER_SOCKET_URL = (argv.serverSocket) ? argv.serverSocket : SERVER_URL;

console.log(PROTOCOL);

console.log('got server url: ', SERVER_URL)

//create string that will eventually be written to file
const out = placeholders(`
export const STORAGE_NAMESPACE = '@EliseStore';
export const SERVER_URL=':serverUrl';
export const SOCKET_SERVER_URL=':socketServerUrl';
`, {
    serverUrl: `${PROTOCOL}://${SERVER_URL}`,
    socketServerUrl: `${PROTOCOL}://${SERVER_SOCKET_URL}`
});

//write to file
fs.writeFile(`${rootPath}/app/config.ts`, out, err => (err ? console.log(err) : console.log('using server config: ', SERVER_URL)))
