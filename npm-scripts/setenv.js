#!/usr/bin/env node
const fs = require('fs');
const rootPath = require('process').cwd();
const argv = require('minimist')(process.argv.slice(2));

const environments = {
    local: {
        server: 'localhost:9999',
        protocol: 'http'
    },
    macbook: {
        server: '192.168.1.148:9999',
        protocol: 'http'
    },
    prod: {
        server: 'elise-service.herokuapp.com',
        protocol: 'https'
    }
}

//get command level arguments
const env = argv.env || 'local';

//create string that will eventually be written to file
let out = '';
for (let configKeyName in environments[env]) {
    out += `eliseRetailer:${configKeyName}=${environments[env][configKeyName]}\n`
}

//write to file
fs.writeFile(`${rootPath}/.npmrc`, out, err => (err ? console.log(err) : console.log('using environment: ', env)))
